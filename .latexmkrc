# Make sure all build artefacts go to a separate directory
$aux_dir = $out_dir = "build";

# Why wouldn't you want a PDF?
$pdf_mode = 1;