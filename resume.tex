\documentclass[a4paper,10pt]{article}

%A Few Useful Packages
\usepackage{marvosym}
\RequirePackage{color,graphicx}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{fullpage}
\usepackage{supertabular} 				% for Grades
\usepackage{titlesec}					% custom \section
\usepackage{enumitem}                   % for custom line spacing in itemize

%Setup hyperref package, and colours for links
\usepackage{hyperref}
\definecolor{linkcolour}{rgb}{0,0.2,0.6}
\hypersetup{colorlinks,breaklinks,urlcolor=linkcolour, linkcolor=linkcolour}

\usepackage[absolute]{textpos}

\setlength{\TPHorizModule}{30mm}
\setlength{\TPVertModule}{\TPHorizModule}
\textblockorigin{2mm}{0.65\paperheight}
\setlength{\parindent}{0pt}

% Lets you insert line breaks inside a tabular cell
\newcommand{\specialcell}[2][c]{\begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}

%CV Sections inspired by:
%http://stefano.italians.nl/archives/26
\titleformat{\section}{\Large\scshape\raggedright}{}{0em}{}[\titlerule]
\titlespacing{\section}{0pt}{3pt}{3pt}
%Tweak a bit the top margin
%\addtolength{\voffset}{-1.3cm}

% Change the top and bottom margins to squeeze everything onto one page
\usepackage[top=1in, bottom=0.7in]{geometry}

\begin{document}

\pagestyle{empty} % non-numbered pages

%--------------------TITLE-------------
\par{\centering
	{\Huge Michael \textsc{Bryan}
		}\bigskip\par}

%--------------------SECTIONS-----------------------------------

\section{Personal Information}

\begin{tabular}{rl}
	\textsc{Email:}   & \href{mailto:consulting@michaelfbryan.com}{consulting@michaelfbryan.com} \\
	\textsc{Web:}     & \url{http://adventures.michaelfbryan.com/}                     \\
	\textsc{GitHub:}  & \url{https://github.com/Michael-F-Bryan/}                      \\
	\textsc{GitLab:}  & \url{https://gitlab.com/Michael-F-Bryan/}                      \\
\end{tabular}

% We're putting lists in tables for a bit. Temporarily change the inter-cell
% height to be as small as possible.
{\renewcommand{\arraystretch}{0}

	\section{Mission Statement}

	To leverage my experience in the open-source community and working
	professionally to make cool things.

	\section{Work History}
	\begin{tabular}{p{0.2\textwidth}p{0.01\textwidth}p{0.7\textwidth}}
		\renewcommand{\arraystretch}{0}
		\textsc{February 2021 \newline - August 2022} &   & \textbf{Software Engineer at Hammer of the Gods}                    \\
			& & Lead engineer working on HOTG's core containerization technology. \\
			& & \begin{itemize}[noitemsep]
			\item Created Rune, a WebAssembly-based declarative containerization technology for machine
				  learning on the edge
			\item Implemented a compiler for Rune, plus runtimes for the browser, mobile, and desktop
			\item Created a Rust framework for custom processing blocks that can be used with Rune
			\item Assisted customers with integrating Rune into their apps
			\item Assisted the frontend for \textit{Forge Studio}, a graphical IDE for defining
				  and building Runes in the browser
			\item Implemented the backend for \textit{Weld Studio}, a Tauri
				  desktop app enabling data analysists to graphically define and
				  execute data processing pipelines locally
			\end{itemize} \\

		\textsc{December 2020 \newline - June 2021} &   & \textbf{Freelance Software Engineer}                    \\
			& & Provided software consulting services to various small businesses in Perth. \\

		\textsc{March 2017 \newline - November 2020} &   & \textbf{Software Engineer at Wintech Engineering}                    \\
		                                           &   & Responsible for the \textit{Wintech Profiler} project, Wintech's
		domain-specific CAD/CAM package for controlling CNC foam cutting
		machines. \\
		                                           &   & \begin{itemize}[noitemsep]
		\item Implemented the 9th version of the package by rewriting from the ground up in C\# and WPF
		\item Contributed 150kloc to the project (120kloc of C\#),
		approx. 2000+ tests, 620 merged feature branches
		\item Managed the product's release to customers, including factory visits
		and training material
		\item Introduced modern software practices like revision control (git
		and GitLab), Continuous Integration, and release automation
		\item Regularly provided technical support to customers in matters
		related to software and the motion control system
		\end{itemize} \\

		\textsc{November 2016 \newline -- February 2017}
		                                           &   & \textbf{Internship at Wintech Engineering}                           \\
		                                           &   & \begin{itemize}[noitemsep]
		\item Created an interface to a 3rd party autonesting program for optimally
		positioning parts to minimise waste
		\item Implemented pathfinding to optimally link multiple nested parts into
		a contiguous cut path
		\item Upgraded the Profiler 8 GUI to incorporate modern components like Ribbons
		and high resolution icons
		\end{itemize} \\
	\end{tabular}

	% Tables are used as tables again
	{\renewcommand{\arraystretch}{1}

		\section{Open-Source Projects}
		\begin{tabular}{p{0.2\textwidth}p{0.01\textwidth}p{0.7\textwidth}}
			\textbf{Rust User \newline Forums}
			  &   & Core member of the Rust community, answering technical
			  questions and teaching new Rustaceans about the language. Received
			  over 4700 likes and solved over 250 threads.

			(\url{https://users.rust-lang.org/u/michael-f-bryan/summary}) \\

			\textbf{Website}
			  &   & An online platform I use for writing about deeply technical
			topics in the field of Software Engineering like WebAssembly,
			application architecture, interoperability between programming languages,
			and computational geometry
			(\url{http://adventures.michaelfbryan.com/})\\

			\textbf{gcode-rs}
			  &   & A g-code parser for use in memory-constrained environments. Primarily used
			by hobbyists implementing their own 3D printers.
			(\url{https://github.com/Michael-F-Bryan/gcode-rs}) \\
		\end{tabular}


		\section{Programming Languages and Technologies}
		\begin{tabular}{p{0.3\textwidth}p{0.01\textwidth}p{0.6\textwidth}}
			\specialcell{
			\textsc{Adept}  \\
			\textit{\small (drop me in a new project and} \\
			\textit{\small I'll be productive immediately)}
			}
			  &   & \textsc{Rust}, \textsc{WebAssembly}, \textsc{TypeScript},
			  \textsc{C\#}, \textsc{Go}, \textsc{Windows GUI Programming (WPF)},
			  \textsc{Python} \textsc{Linux}
			\textit{\small (Arch Linux user for 4 years)} \\
			\\
			\textsc{Confident}
			  &   & \textsc{C++}, \textsc{Embedded C},
			\textsc{Web UIs using JavaScript and Vue.js or React.js},
			\textsc{SQL Databases (Postgres, Sqlite)}, \textsc{Docker and Micro Services},
			\textsc{Delphi Pascal} \\
		\end{tabular}


		\section{Education}
		\begin{tabular}{p{0.3\textwidth}p{0.7\textwidth}}
			\textsc{2013 - 2018} & \textbf{Bachelor of Mechanical Engineering} \\
			                     & Curtin University                                        \\
			\textsc{2012}        & \textbf{Australian Tertiary Admission Rank - 99.90}      \\
			                     & Scotch College                                           \\
		\end{tabular}


		\section{Additional Details}

		\begin{itemize}
			\item Team leader in the \textit{Communication Support Unit}, a
			      \textit{State Emergency Service} unit which provides large incidents with
			      radio communications and mapping services. Member since November 2018.

			\item Member of the \textit{Curtin Motorsport Team} from 2014 to 2018.
			      Held the \textit{Assistant Project Manager} role during 2015 and
			      was involved in the various software projects as part of the
			      Electrical Team
		\end{itemize}

		\section{References}
		\begin{tabular}{p{0.3\textwidth}p{0.7\textwidth}}
			\textbf{Kartik Thakore} \\
			\textsc{Relationship}   & CTO and direct supervisor at Hammer of the Gods. \\
			\textsc{Email Address} & \textit{kartik@thakore.ai}                        \\

			& \\
			\textbf{Bradley Eccles} \\
			\textsc{Relationship}   & Lead Service Engineer at Wintech Engineering. Acts as a   \\
			                        & bridge between the developers and end users, providing    \\
			                        & assistance in project direction and user experience       \\
			\textsc{Contact Number} & \textit{0439 981 016}                                     \\
		\end{tabular}

\end{document}
