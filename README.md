# My Resume

[![pipeline status](https://gitlab.com/Michael-F-Bryan/resume/badges/master/pipeline.svg)](https://gitlab.com/Michael-F-Bryan/resume/commits/master)

(**[View Online](https://michael-f-bryan.gitlab.io/resume/resume.pdf)**)

This is a repository containing my resume and some other useful documents. It
used to live at [Michael-F-Bryan/uni-work][uni-work]... but now I'm no longer a
uni student, so here it is.

## Getting Started

To avoid bloating my laptop installation with unnecessary software, and ensure
a consistent build/environment, this uses [a popular docker image][img] with
everything you'll need.

First, make sure [docker](https://www.docker.com/) is running: 

```console
$ sudo systemctl start docker
$ sudo systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/lib/systemd/system/docker.service; disabled; vendor preset: enabled)
   Active: active (running) since Sun 2019-08-11 16:27:04 AWST; 1 day 4h ago
     Docs: https://docs.docker.com
 Main PID: 13328 (dockerd)
    Tasks: 25
   Memory: 380.1M
   CGroup: /system.slice/docker.service
           └─13328 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
```

Then you can invoke `latexmk` inside a docker container to (this may take a
while on your first run because it needs to download the image).

```console
$ docker run --rm -i --user="$(id -u):$(id -g)" --net=none -v "$PWD":/data blang/latex:ubuntu latexmk
```

Setting up volumes (if you've used *VirtualBox* before, they're kinda like 
shared folders) and user IDs can be a bit of a mouthful, so the GitHub repo 
behind `blang/latex:ubuntu` provides a helper script.

```console
$ ./latexdockercmd.sh latexmk -quiet resume.tex
$ ls build
resume.aux  resume.fdb_latexmk  resume.fls  resume.log  resume.out  resume.pdf
$ xdg-open build/resume.pdf
```

> **Note:** When LaTeX sees a relative path, it'll resolve that path relative to
> the **current directory**, not relative to the file's directory. If you get
> a *"File Not Found"* error, try to run the build from this repo's root 
> directory.

[uni-work]: https://gitlab.com/Michael-F-Bryan/uni-work/tree/master
[img]: https://hub.docker.com/r/blang/latex/
